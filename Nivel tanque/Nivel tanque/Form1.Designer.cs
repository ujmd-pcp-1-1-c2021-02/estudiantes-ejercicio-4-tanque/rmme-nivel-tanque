﻿namespace Nivel_tanque
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnltanque = new System.Windows.Forms.Panel();
            this.pnlliquido = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pbparo = new System.Windows.Forms.PictureBox();
            this.pbvaciar = new System.Windows.Forms.PictureBox();
            this.pbllenar = new System.Windows.Forms.PictureBox();
            this.btparar = new System.Windows.Forms.Button();
            this.btvaciar = new System.Windows.Forms.Button();
            this.btllenar = new System.Windows.Forms.Button();
            this.tmrllenar = new System.Windows.Forms.Timer(this.components);
            this.tmrvaciar = new System.Windows.Forms.Timer(this.components);
            this.tmrluces = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbparo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbvaciar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbllenar)).BeginInit();
            this.SuspendLayout();
            // 
            // pnltanque
            // 
            this.pnltanque.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pnltanque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnltanque.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnltanque.Location = new System.Drawing.Point(318, 168);
            this.pnltanque.Name = "pnltanque";
            this.pnltanque.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pnltanque.Size = new System.Drawing.Size(29, 243);
            this.pnltanque.TabIndex = 0;
            // 
            // pnlliquido
            // 
            this.pnlliquido.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.pnlliquido.Location = new System.Drawing.Point(318, 168);
            this.pnlliquido.Name = "pnlliquido";
            this.pnlliquido.Size = new System.Drawing.Size(29, 78);
            this.pnlliquido.TabIndex = 0;
            this.pnlliquido.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlliquido_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(285, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(424, 399);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.Controls.Add(this.pbparo);
            this.panel2.Controls.Add(this.pbvaciar);
            this.panel2.Controls.Add(this.pbllenar);
            this.panel2.Controls.Add(this.btparar);
            this.panel2.Controls.Add(this.btvaciar);
            this.panel2.Controls.Add(this.btllenar);
            this.panel2.Location = new System.Drawing.Point(21, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 207);
            this.panel2.TabIndex = 2;
            // 
            // pbparo
            // 
            this.pbparo.BackColor = System.Drawing.Color.Maroon;
            this.pbparo.Location = new System.Drawing.Point(128, 156);
            this.pbparo.Name = "pbparo";
            this.pbparo.Size = new System.Drawing.Size(44, 25);
            this.pbparo.TabIndex = 5;
            this.pbparo.TabStop = false;
            // 
            // pbvaciar
            // 
            this.pbvaciar.BackColor = System.Drawing.Color.DarkGreen;
            this.pbvaciar.Location = new System.Drawing.Point(128, 94);
            this.pbvaciar.Name = "pbvaciar";
            this.pbvaciar.Size = new System.Drawing.Size(44, 25);
            this.pbvaciar.TabIndex = 4;
            this.pbvaciar.TabStop = false;
            // 
            // pbllenar
            // 
            this.pbllenar.BackColor = System.Drawing.Color.DarkGreen;
            this.pbllenar.Location = new System.Drawing.Point(128, 31);
            this.pbllenar.Name = "pbllenar";
            this.pbllenar.Size = new System.Drawing.Size(44, 25);
            this.pbllenar.TabIndex = 3;
            this.pbllenar.TabStop = false;
            // 
            // btparar
            // 
            this.btparar.Location = new System.Drawing.Point(23, 150);
            this.btparar.Name = "btparar";
            this.btparar.Size = new System.Drawing.Size(88, 31);
            this.btparar.TabIndex = 2;
            this.btparar.Text = "Parar";
            this.btparar.UseVisualStyleBackColor = true;
            this.btparar.Click += new System.EventHandler(this.btparar_Click);
            // 
            // btvaciar
            // 
            this.btvaciar.Location = new System.Drawing.Point(23, 88);
            this.btvaciar.Name = "btvaciar";
            this.btvaciar.Size = new System.Drawing.Size(88, 31);
            this.btvaciar.TabIndex = 1;
            this.btvaciar.Text = "Vaciar";
            this.btvaciar.UseVisualStyleBackColor = true;
            this.btvaciar.Click += new System.EventHandler(this.btvaciar_Click);
            // 
            // btllenar
            // 
            this.btllenar.Location = new System.Drawing.Point(23, 26);
            this.btllenar.Name = "btllenar";
            this.btllenar.Size = new System.Drawing.Size(88, 31);
            this.btllenar.TabIndex = 0;
            this.btllenar.Text = "Llenar";
            this.btllenar.UseVisualStyleBackColor = true;
            this.btllenar.Click += new System.EventHandler(this.btllenar_Click);
            // 
            // tmrllenar
            // 
            this.tmrllenar.Interval = 50;
            this.tmrllenar.Tick += new System.EventHandler(this.tmrllenar_Tick);
            // 
            // tmrvaciar
            // 
            this.tmrvaciar.Interval = 50;
            this.tmrvaciar.Tick += new System.EventHandler(this.tmrvaciar_Tick);
            // 
            // tmrluces
            // 
            this.tmrluces.Tick += new System.EventHandler(this.tmrluces_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnlliquido);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnltanque);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbparo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbvaciar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbllenar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnltanque;
        private System.Windows.Forms.Panel pnlliquido;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbparo;
        private System.Windows.Forms.PictureBox pbvaciar;
        private System.Windows.Forms.PictureBox pbllenar;
        private System.Windows.Forms.Button btparar;
        private System.Windows.Forms.Button btvaciar;
        private System.Windows.Forms.Button btllenar;
        private System.Windows.Forms.Timer tmrllenar;
        private System.Windows.Forms.Timer tmrvaciar;
        private System.Windows.Forms.Timer tmrluces;
    }
}

