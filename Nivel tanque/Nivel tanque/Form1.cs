﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nivel_tanque
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btllenar_Click(object sender, EventArgs e)
        {
            tmrllenar.Enabled = true;
            tmrvaciar.Enabled = false;
        }

        private void btvaciar_Click(object sender, EventArgs e)
        {
            tmrvaciar.Enabled = true;
            tmrllenar.Enabled = false;
        }

        private void tmrllenar_Tick(object sender, EventArgs e)
        {
            if (pnlliquido.Height < pnltanque.Height)
            {
                pnlliquido.Height += 2;
            }
            else
            {
                tmrllenar.Enabled = false;
            }
        }

        private void tmrvaciar_Tick(object sender, EventArgs e)
        {
            if (pnlliquido.Height > 0)
            {
                pnlliquido.Height -= 2;
            }
            else
            {
                tmrvaciar.Enabled = false;
            }
        }

        private void tmrluces_Tick(object sender, EventArgs e)
        {
            if (tmrllenar.Enabled == true)
            {
                pbllenar.BackColor = Color.DarkGreen;
            }
            else
            {
                pbllenar.BackColor = Color.Lime;
            }

            if (tmrvaciar.Enabled == true)
            {
                pbvaciar.BackColor = Color.DarkGreen;
            }
            else
            {
                pbvaciar.BackColor = Color.Lime;
            }

            if (tmrllenar.Enabled == false && tmrvaciar.Enabled == false)
            {
                pbparo.BackColor = Color.Maroon;
            }
            else
            {
                pbparo.BackColor = Color.Red;
            }
        }

        private void btparar_Click(object sender, EventArgs e)
        {
            tmrllenar.Enabled = false;
            tmrvaciar.Enabled = false;
        }

        private void pnlliquido_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
